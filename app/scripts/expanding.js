(function($) {
    'use strict';
    // check if elemens is visible in viewport ($el.isVisible())
    var $w = $(window);
    $.fn.isVisible = function(partial, hidden, direction) {
        if (this.length < 1) {
            return;
        }

        var $t = this.length > 1 ? this.eq(0) : this,
            t = $t.get(0),
            vpWidth = $w.width(),
            vpHeight = $w.height(),
            direction = (direction) ? direction : 'both',
            clientSize = hidden === true ? t.offsetWidth * t.offsetHeight : true;

        if (typeof t.getBoundingClientRect === 'function') {

            // Use this native browser method, if available.
            var rec = t.getBoundingClientRect(),
                tViz = rec.top >= 0 && rec.top < vpHeight,
                bViz = rec.bottom > 0 && rec.bottom <= vpHeight,
                lViz = rec.left >= 0 && rec.left < vpWidth,
                rViz = rec.right > 0 && rec.right <= vpWidth,
                vVisible = partial ? tViz || bViz : tViz && bViz,
                hVisible = partial ? lViz || rViz : lViz && rViz;

            if (direction === 'both')
                return clientSize && vVisible && hVisible;
            else if (direction === 'vertical')
                return clientSize && vVisible;
            else if (direction === 'horizontal')
                return clientSize && hVisible;
        } else {

            var viewTop = $w.scrollTop(),
                viewBottom = viewTop + vpHeight,
                viewLeft = $w.scrollLeft(),
                viewRight = viewLeft + vpWidth,
                offset = $t.offset(),
                _top = offset.top,
                _bottom = _top + $t.height(),
                _left = offset.left,
                _right = _left + $t.width(),
                compareTop = partial === true ? _bottom : _top,
                compareBottom = partial === true ? _top : _bottom,
                compareLeft = partial === true ? _right : _left,
                compareRight = partial === true ? _left : _right;

            if (direction === 'both')
                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop)) && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
            else if (direction === 'vertical')
                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop));
            else if (direction === 'horizontal')
                return !!clientSize && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
        }
    };

})(jQuery);

(function($) {
    $.fn.expandByRow = function(options) {
        var settings = $.extend({
            offsetTop: 0,
            swiper: false,
            swiperSettings: {
                mode: 'horizontal',
                calculateHeight: true,
                speed: 600,
                loop: true
            },
            equalHeightItem: false,
            customEqualHeight: false
        }, options);

        var wrapper = this.selector,
            listItems = wrapper + ' .expandItemList',
            listItemsDetail = wrapper + ' .expandItemDetailList',
            item = listItems + ' .expandItem:visible',
            itemDetail = '.expandItemDetail',
            infoClass = listItems + ' .exp-item-info',
            logoBlock = listItems + ' .exp-item-logo',
            activeItem = item + '.active',
            swiperContainer = itemDetail + ' .swiper-container',
            expandItemSwiper = undefined,
            wWidth = $(window).width(),
            swiperNavigatePrev = itemDetail + ' .prev',
            swiperNavigateNext = itemDetail + ' .next',
            resize = 0,

            countLastRow = function(item) {
                var itemsInLastRow = 0,
                    lastItemOffset = $(item).last().offset().top;
                $(item).each(function() {
                    var _selfOffset = $(this).offset().top;
                    if (_selfOffset === lastItemOffset) {
                        itemsInLastRow++;
                    }
                });
                return itemsInLastRow;
            },

            destroyItemDetail = function() {
                $(listItemsDetail).append($(itemDetail));
                $(itemDetail).removeAttr('style');
                if (settings.swiper === true) {
                    $(itemDetail).find('.swiper-wrapper').removeAttr('style');
                    $(itemDetail).find('.swiper-slide').removeAttr('style');
                    $(itemDetail).find('.swiper-slide-duplicate').remove();
                    if (expandItemSwiper !== undefined) {
                        expandItemSwiper.destroy();
                        expandItemSwiper = undefined;
                    }
                }
            },

            scrollTopAfterShowDetail = function() {
                var offsetTop = settings.offsetTop;
                // if (offsetTop && offsetTop !== false && $.isNumeric(settings.offsetTop) !== true) {
                //     offsetTop = $(settings.offsetTop).css('position') === 'fixed' ? $(settings.offsetTop).outerHeight() : 0;
                // }
                if ($.isNumeric(offsetTop) !== true) {
                    offsetTop = $(offsetTop).css('position') === 'fixed' ? $(offsetTop).outerHeight() : 0;
                }
                var _scrollTop = $(activeItem).offset().top - offsetTop - 10;
                setTimeout(function() {
                    if ($(activeItem).offset().top - $(window).scrollTop() < offsetTop || $(activeItem).isVisible() === false || $(itemDetail).isVisible() === false) {
                        $('html, body').animate({
                            scrollTop: _scrollTop
                        }, 300);
                    }
                }, 10);
            },

            initPortfolioSwiper = function(target) {
                if (settings.swiper === true) {
                    var arrowNavigate = $(itemDetail + ' .prev,' + itemDetail + ' .next');
                    // settings.swiperSettings.slidesPerView = ($(window).width() <= 480) ? 1 : 2;
                    //set max-height for slide

                    if ($(target + ' .swiper-container .swiper-slide').length > 1) {
                        arrowNavigate.show();
                        if (expandItemSwiper === undefined) {
                            expandItemSwiper = new Swiper(target + ' .swiper-container', settings.swiperSettings);
                        } else {
                            // expandItemSwiper.params.slidesPerView = settings.swiperSettings.slidesPerView;
                            expandItemSwiper.reInit();
                            setTimeout(function() {
                                if (expandItemSwiper) {
                                    expandItemSwiper.reInit();
                                }
                            }, 800);
                        }
                    } else {
                        arrowNavigate.hide();
                    }
                }
            };
        var action = {
            init: function() {
                // action click on item to expand detail
                $('body').on('click', item, function() {
                    var target = $(this).data('target'),
                        _selfOffset = $(this).offset().top,
                        nextAllItems = $(this).nextAll(item),
                        itemsInLastRow = countLastRow(item);
                    if ($(this).hasClass('active')) {
                        $(item).removeClass('active');
                        $(target).css('min-height', 0);
                        $(target).stop().slideUp(500, function() {
                            $(listItemsDetail).append($(itemDetail));
                            destroyItemDetail();
                            if (typeof settings.collapsingDone == 'function') {
                                settings.collapsingDone.call(this);
                            }
                        });
                    } else {
                        $(item).removeClass('active');
                        $(this).addClass('active');
                        destroyItemDetail();
                        if (nextAllItems.length < itemsInLastRow) {
                            // click in last row
                            $(item).last().after($(target));
                            if ($(target).length > 0) {
                                setTimeout(function() {
                                    initPortfolioSwiper(target);
                                }, 100);
                                $(target).stop().slideDown(500, function() {
                                    scrollTopAfterShowDetail();
                                    if (typeof settings.expandingDone == 'function') {
                                        settings.expandingDone.call(this);
                                    }
                                });
                            }
                        } else {
                            // not click in last row
                            var _selfOffset = $(this).offset().top;
                            for (var i = 0; i < nextAllItems.length; i++) {
                                var nextItem = nextAllItems[i],
                                    nextItemOffset = $(nextItem).offset().top - 1; // trick rounding number for firefox, IE
                                if (_selfOffset < nextItemOffset) {
                                    $(nextItem).prev().after($(target));
                                    if ($(target).length > 0) {
                                        setTimeout(function() {
                                            initPortfolioSwiper(target);
                                            $(target).css('max-height', $(target + ' .expandItemDetailWrapper').height());
                                        }, 100);
                                        $(target).stop().slideDown(500, function() {
                                            scrollTopAfterShowDetail();
                                            $(target).css('max-height', 'none');
                                            if (typeof settings.expandingDone == 'function') {
                                                settings.expandingDone.call(this);
                                            }
                                        });
                                    }
                                    return false;
                                }
                            }
                        }
                    }
                });
            },

            resizeExpandItem: function() {
                if ($(activeItem).length) {
                    $(itemDetail).hide();
                    var nextAllItems = $(activeItem).nextAll(item),
                        _selfOffset = $(activeItem).offset().top,
                        target = listItems + ' ' + itemDetail,
                        itemsInLastRow = countLastRow(item);
                    if (nextAllItems.length < itemsInLastRow) {
                        // active in last row
                        $(item).last().after($(target));
                        setTimeout(function() {
                            initPortfolioSwiper(target);
                        }, 100);
                        $(target).slideDown(500, function() {
                            scrollTopAfterShowDetail();
                        });
                    } else {
                        // not active in last row
                        for (var i = 0; i < nextAllItems.length; i++) {
                            var nextItem = nextAllItems[i],
                                nextItemOffset = $(nextItem).offset().top;
                            if (_selfOffset < nextItemOffset) {
                                $(nextItem).before($(target));
                                setTimeout(function() {
                                    initPortfolioSwiper(target);
                                }, 100);
                                $(target).slideDown(500, function() {
                                    scrollTopAfterShowDetail();
                                });
                                return false;
                            }
                        }
                    }
                }
            },

            equalHeight: function(obj) {
                var widthTarget = 0;
                if ($(obj).length) {
                    $(obj).height('auto');
                    // widthTarget = (notRunMobile === true) ? 768 : 0;
                    if ($(window).width() >= widthTarget) {
                        var currentTallest = 0,
                            currentRowStart = 0,
                            rowDivs = [],
                            currentDiv = 0,
                            $el,
                            topPosition = 0;
                        $(obj).each(function() {
                            if ($(this).is(':visible') === true) {
                                $el = $(this);
                                topPosition = $el.offset().top;
                                if (currentRowStart !== topPosition) {
                                    for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                                        rowDivs[currentDiv].innerHeight(currentTallest);
                                    }
                                    rowDivs = [];
                                    currentRowStart = topPosition;
                                    currentTallest = $el.innerHeight();
                                    rowDivs.push($el);
                                } else {
                                    rowDivs.push($el);
                                    currentTallest = (currentTallest < $el.innerHeight()) ? ($el.innerHeight()) : (currentTallest);
                                }
                                for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                                    rowDivs[currentDiv].innerHeight(currentTallest);
                                }
                            }
                        });
                    }
                }
            }
        };

        action.init();

        if (settings.customEqualHeight !== false) {
            action.equalHeight(settings.customEqualHeight);
        }
        if (settings.equalHeightItem === true) {
            action.equalHeight(item);
        }

        $(window).load(function() {
            if (settings.customEqualHeight !== false) {
                action.equalHeight(settings.customEqualHeight);
            }
            if (settings.equalHeightItem === true) {
                action.equalHeight(item);
            }
        });

        $('body').on('click', swiperNavigatePrev, function() {
            expandItemSwiper.swipePrev();
        });
        $('body').on('click', swiperNavigateNext, function() {
            expandItemSwiper.swipeNext();
        });

        $(window).resize(function() {
            var _self = $(this);
            resize++;
            setTimeout(function() {
                resize--;
                if (resize === 0) {
                    // Done resize ...
                    if (_self.width() !== wWidth) {
                        wWidth = _self.width();
                        // done resizing width
                        action.resizeExpandItem();
                        if (settings.customEqualHeight !== false) {
                            action.equalHeight(settings.customEqualHeight);
                        }
                        if (settings.equalHeightItem === true) {
                            action.equalHeight(item);
                        }
                    }
                }
            }, 100);
        });
        return action;
    };
})(jQuery);

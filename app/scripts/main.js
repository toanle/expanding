$(document).ready(function() {
    var expanding = $('.expandItemWrapper').expandByRow({
        offsetTop: 'header',
        swiper: true,
        equalHeightItem: true,
        expandingDone: function() {
            console.log('expanding is done');
        },
        collapsingDone: function() {
            console.log('collapsing is done');
        }
    });
});
